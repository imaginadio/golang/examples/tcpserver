package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("usage: %s port_number", os.Args[0])
		return
	}
	port := ":" + os.Args[1]

	l, err := net.Listen("tcp4", port)
	if err != nil {
		panic(err)
	}

	for {
		fmt.Println("Ready to serve new connection...")
		conn, err := l.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}

		go handleConn(conn)
	}
}

func handleConn(conn net.Conn) {
	defer conn.Close()
	fmt.Println("connection established")
	connReader := bufio.NewReader(conn)
	for {
		gotStr, err := connReader.ReadString('\n')
		if err != nil {
			fmt.Println(err)
			return
		}

		gotStr = strings.TrimSpace(gotStr)
		if gotStr == "quit" {
			fmt.Println("close the connection and quitting...")
			return
		}

		fmt.Println(gotStr)
		_, err = conn.Write([]byte("Sup!\n"))
		if err != nil {
			fmt.Println(err)
		}
	}
}
